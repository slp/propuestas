---
layout: 2020/post
section: propuestas
category: devrooms
title: Privacidad y soberanía digital
state: cancelled
---

En un mundo con cada vez más tecnología, cada día tenemos menos poder sobre lo que ocurre con nuestra información personal.  

El poder que el Software Libre nos dio para con nuestros dispositivos, la economía de la vigilancia nos lo está arrebatando con la centralización de servicios en manos de empresas cuyo modelo de negocio es venderte.  

En consecuencia, nuestra soberanía democrática, social e individual se están viendo mermadas y coartadas al permitir (a veces sin apenas pensarlo) que se traten nuestros datos, muchas veces de forma desproporcionada, y con consecuencias que todavía no podemos ni anticipar.

-   ¿Qué podemos hacer al respecto?
-   ¿De qué herramientas y servicios alternativos disponemos?
-   ¿Cómo educamos a la sociedad para que quiera defenderse?

Para esta sala buscamos contribuciones que indaguen en los problemas que surgen respecto a estos temas, y también sobre proyectos y alternativas que respeten los derechos fundamentales de las personas y que nos hagan más soberanos o conscientes con la tecnología, de forma general.

Nos interesan mucho los siguientes temas, pero no dudes en proponer otros que puedan encajar aunque no estén especificados en la lista. ¡Estamos totalmente abiertos a propuestas!

-   Tecnologías, proyectos e iniciativas basadas en la privacidad:
    -   Cómo crear conciencia ciudadana al respecto
    -   Redes anónimas, Friend-to-friend, o Dark Nets
    -   Sistemas operativos pro-privacidad
    -   Infraestructura y tecnología para la anonimidad en la red
    -   Seguridad operacional: como reducir la cantidad de info. que se recopila de ti?
    -   Modelos de amenaza: hasta dónde debería de llegar tu paranoia?
    -   Pasos para minimizar el impacto en una brecha de privacidad
-   Tecnologías, proyectos e iniciativas basadas en la soberanía digital
    -   Como crear conciencia ciudadana al respecto
    -   Redes descentralizadas y comunitarias, pro-soberanía digital
    -   Sistemas de votación electrónica con garantías democráticas
    -   Tecnología para permitir a la ciudadanía el control en los procesos democráticos
    -   Proyectos para incrementar la Transparencia en organizaciones
    -   Sobre control individual o social de los datos personales
    -   Sobre la elaboración de perfiles ciudadanos y su impacto
    -   Mecanismos que no requieran confianza para realizar operaciones seguras
    -   Comunidades de self-hosting
    -   Soluciones para el self-hosting de tus propios servicios
    -   Redes, algoritmos y protocolos de red descentralizados
    -   Aplicaciones pro-soberanía tecnológica para servicios finales, como mensajería instantánea, almacenamiento de backups, compartición de documentos o páginas web, colaboración en tiempo real...

## Comunidad o grupo que lo propone

**Trackula**. Bajo este nombre nos reunimos un grupo de hacktivismo y concienciación social interesados en las cuestiones de privacidad, descentralización, software libre y soberanía de la información personal y social.

No nos hemos registrado como Asociación todavía,  aunque podéis conocer a nuestros fundadores también a través de GPUL, y bajo unas u otras siglas tenemos experiencia en la realización de actividades al respecto.

También estamos desarrollando software libre dentro de [un sub-proyecto](https://www.ngi.eu/opencalls/ledger/) de [Next Generation Internet](https://www.ngi.eu) del Programa Marco Horizonte 2020.

Además de experiencia en la realización de conferencias (algunas internacionales, como GUADEC, [Akademy](https://akademy.kde.org/2015) o [XDC](https://xdc2018.x.org/)), también hemos dinamizado espacios como el [Summerlab de Hirikilabs](https://www.tabakalera.eu/en/summerlab-2018-meeting-citizen-research-civic-use-of-technology), y participado en talleres como [Visualizar'17](https://www.youtube.com/watch?v=ixpbzv2pRTA), o charlas como [Watching Them Watching Us](https://www.youtube.com/watch?v=O-x4jQ2UNOw) en FOSDEM'19.

Hemos sido los promotores de esta misma actividad el año pasado.

### Contactos

-   **Santiago Saavedra**: @ssaavedra.eu
-   **Pablo Castro**: @castrinho8
-   **Genoveva Galarza**: @genocation
-   **Sofía Prósper**: @sofipros

Estamos abiertos a incorporar a nuestra candidatura a más personas que compartan estas inquietudes y que quieran participar de la propia organización de la devroom.

## Público objetivo

A todo el mundo.

## Tiempo

En principio, un día.

## Día

Como promotores, preferimos que la devroom ocurra el viernes, pero si la organización de esLibre lo permite, estamos abiertos a que los candidatos nos sugieran cambiarla al sábado.

## Formato

Se aceptarán charlas de 20+5 minutos, se organizará una mesa redonda al final del día, y se dejarán espacios con el propósito de generar una "desconferencia por intervalos" hilada por las temáticas de las charlas.

## Comentarios

Prevemos utilizar la misma plataforma que el año pasado para gestionar las propuestas de contribuciones a la sala. Es un OSEM, y está disponible en <https://osem.trackula.org/admin/conferences/eslibre-2020-privacy>

Si se acepta esta propuesta, lo haremos oficial promocionándolo a través de esta página: <https://trackula.org/en/page/cfp-eslibre-2020/>

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
-   [x] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
