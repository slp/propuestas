---
layout: 2020/post
section: propuestas
category: talks
title: Máquinas Virtuales ligeras para serverless y containers
---

Los servicios basados en la nube han generalizado un nuevo tipo de carga de trabajo distinto al que se solía ver en los entornos de Virtualización tradicionales, lo que ha empujado a las plataformas de Virtualización basadas en Software Libre a evolucionar y optimizarse para cubrir mejor las necesidades de la misma.

Esta charla es una versión en Español de la charla "Lightweight VMs for serverless and containers", que presenté en la pasada DevConf.cz 2020. Está además actualizada para cubrir los nuevos desarrollos que han tenido lugar en lo que llevamos de año.

## Formato de la propuesta

Indicar uno de estos:

* [x] &nbsp;Charla (25 minutos)
* [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

Los servicios basados en la nube han generalizado un nuevo tipo de carga de trabajo compuesto, fundamentalmente, por procesos efímeros y de corta vida. La naturaleza de estas cargas de trabajo es muy distinta a las de las que se solían ejecutar en los entornos de Virtualización tradicionales, en los que habitualmente las Máquinas Virtuales son persistentes y permanecen activas durante largos periodos de tiempo, incluso superando el propio tiempo de vida del Host mediante el uso de migraciones en caliente.

Estos nuevos tipos de carga de trabajo implican nuevas y distintas necesidades en las plataformas que los sustentan, las cuales requieren la implementación de nuevas técnicas de optimización, lo que ha provocado la aparición de nuevos VMMs (Virtual Machine Monitors) y cambios en los ya existentes, junto al desarrollo de nuevas estrategias de aislamiento.

En esta charla trataré de explicar por qué las plataformas de Virtualización están evolucionando para adaptarse a los nuevos tipos de cargas de trabajo y cómo lo están haciendo, repasando algunos de los desarrollos más importantes que han tenido lugar en varios proyectos de Software Libre del ámbito.

## Público objetivo

A gente con perfil técnico e interés en aprender más sobre el estado del arte de la Virtualización orientada a la Computación en la Nube.

## Ponente(s)

Mi nombre es **Sergio López** y trabajo como desarrollador de software. Estoy especializado en sistemas operativos y tecnologías de virtualización.

- <https://keybase.io/slpnix>

He presentado la versión en inglés de esta charla en la pasada DevConf.cz 2020, y presenté "Introduction to QEMU/KVM debugging" en DevConf.cz 2018.

### Contacto(s)

* **Sergio López**: slp @ GitLab

## Comentarios

Ninguno.

## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
