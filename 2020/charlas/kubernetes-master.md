---
layout: 2020/post
section: propuestas
category: talks
title: Kubernetes con un master
---

No, ¡yo no soy el master! Repasando las buenas practicas de Kubernetes siempre vemos la indicación de tener los nodos
maestros en alta disponibilidad. Pero... ¿realmente es necesario?

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

Kubernetes es uno de los proyectos open source con más fama y uso en todo el mundo y en todo tipo de proyectos.

Las recomendaciones a la hora de desplegar uno son claras: Crea este con tres o cinco nodos maestros para tener HA _(High Availability o alta disponibilidad)_. Pero no todo tiene que estar en alta disponibilidad. Repasaremos consideraciones, recomendaciones y ayudaremos a tomar la decisión correcta a la hora de crear estas infraestructuras.

Si da tiempo experimentaremos como es la actualización de un clúster de Kubernetes de un solo nodo maestro mientras que la aplicación sigue funcionando.

## Público objetivo

Aquellos DevOps/Devs/Sistemas/Inquietos que han oído hablar de Kubernetes o bien estén usándolo y quieran conocer estas consideraciones a la hora de aplicar Kubernetes en un proyecto.

## Ponente(s)

**Ángel Barrera**, Kubernetes Engineer @ [SIGHUP](https://sighup.io/).

### Contacto(s)

-   **Ángel Barrera**: [@angelbarrera92](https://twitter.com/angelbarrera92)

## Comentarios

Necesito proyector y enchufe.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
