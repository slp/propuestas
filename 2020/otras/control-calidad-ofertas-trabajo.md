---
layout: 2020/post
section: propuestas
category: others
title: Presentación de proyecto&#58 Control de calidad en las ofertas de trabajo
state: confirmed
---

Últimamente las ofertas de trabajo son bastante vacías. Se dan situaciones absurdas como no saber para qué cliente vas a trabajar o cuántos días de vacaciones vas a recibir. ¿Merece la pena una oferta de la compañía de mis sueños si no sé qué quieren para su proyecto?

## Tipo de actividad

Debate y propuestas de mejora sobre el proyecto [qa-empleo](https://github.com/RafaelAybar/qa-empleo).

## Descripción

Viendo desde el punto de vista del empleador, ¿por qué nadie me responde a mis ofertas de trabajo? Si soy de una empresa de selección de personal, ¿cómo le hago entender a mi cliente que ha de darme más detalles?

Por ello necesitamos una manera de hacer ver a los demás desde un punto de vista matemático si una oferta es buena. Para ello desarrollé el proyecto [qa-empleo](https://github.com/RafaelAybar/qa-empleo).

El proyecto no está completo del todo, me queda usar la API de LinkedIn para obtener la rotación histórica (ratio de empleados que abandonan la empresa) de forma automática.

En las mejoras que por ejemplo me gustaría introducir, es cruzar datos con un portal de datos abierto para determinar si se está pagando por debajo de mercado o no, generar un [diagrama de araña](https://en.wikipedia.org/wiki/File:Spider_Chart2.jpg) que facilite ver los puntos fuertes de la oferta, etc.

## Público objetivo

A cualquier persona que tenga conocimientos en Python 3 y quiera colaborar en el proyecto.

## Ponente(s)

**Rafael Aybar Segura**, técnico devops que cacharrea cosas muy básicas de Python.

### Contacto(s)

-   **Rafael Aybar**: rafaelaybar at gmail dot com

## Requisitos para los asistentes

En principio no necesitan nada. Es simplemente una tormenta de ideas para mejorar la aplicación.

## Requisitos para la organización

Una sala, un proyector y sillas.

## Comentarios

Ninguno.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
